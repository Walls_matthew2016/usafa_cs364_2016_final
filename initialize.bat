REM install node packages
call npm install

REM npm install turns ECHO OFF, so commands are no longer visible
ECHO ON

REM create MongoDB database mydb, collection users, and load data from file
mongoimport -v --db stocksdb --collection users --type json --file server/seed/users-seed.json --jsonArray --drop
