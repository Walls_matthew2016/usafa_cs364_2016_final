/**
 * Created by Warren.Watkinson on 4/15/2016.
 */
var express = require('express')
var router = express.Router();
var path = require('path');
var authUtil = require('../services/authUtil')

var users = require(path.join(__dirname,'users'));

var register = require('./register');
var login = require('./login')

router.use('/users',users);
router.use('/register',register);

router
  .use('/register', register)
  .use('/login',login)

  .get('/', function(req, res) {
    res.render('index');
  })

  .get('/dashboard', authUtil.requireLogin('/login'), function(req,res) {
    console.log('dashboard counter: ' + req.session.counter);
    res.render('login', {counter: req.session.counter || 0});
  })

module.exports = router;
