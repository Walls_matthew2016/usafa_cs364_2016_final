`README.md Template
```md
### Project Title: Daily Needs
### Team Members:
+ C1C Matthew Walls
+ C2C Alex Ververis
+ C2C Terrance Oliveri-Williams

### Description
Our project will be a 4 page website that used MySQL databse system. We will use all of the required technolgies and run our 5 web pages. It will be initially
set up where the user logs in with his information(username and password), which will be secure and remembered for future log ins. After logging int he user will have 5 options to pick from. These options will be Stock Market, Weather,
News Stories, and Traffic. These 4 items are what everyone would usually want to see when they wake up or throughout the day, thus each persons log in will have customized information of their own choosing. For Stock Market they will
put in the stocks they wish to see dialy, to include increased or decreased price, a table that shows them how much they have made or lost(after inputing their paid price) depending that that days current price. For Weather they will input
their location initially and it will show them the weather for the day and the following 3 days. New Stories will show the person the Top 5 news stories of the day. For this one there is no customization just links to the top articles. Traffic
will again use the persons location and destination to find the current traffic updates that are avaliable. The special feature from for our project will be acuiring data from a web service. This will be used 3 times to obtain data needed
for the Weather, Traffic, and Stock Market pages to give them valued information. The top 5 news stories will also be pulled, but only have a link and not change the data in any way.

### Installation Instructions
The only installation instruction set currently are the following ones:

+ Requires MongoDB/MySQL Server to be running
+ `initialize.bat` to resolve dependencies and to seed the MongoDB/MySQL server
+ `npm start` to begin the application

### Functionality Shortfalls
Currently there are no specification shortfalls, but I envision we will struggle to secure the data to the maximum level possible.

### Specification
1. Application Requirements
    a. malicious data from the user will not be accepted
    b. Unknown data will be known with a warning and asked to re-do their input
    c. The four pages along with an initial page will display a visually appealing
    d. Any errors within the databse or system will be displayed base on the problem IE if the stock market is closed, it will show the pervious days prices

### Specification
    1. List your application requirements here
        1. Malicious data from the user will not be accepted
        2. Unknown data will be known with a warning and asked to re-do their input by either a pop up window or in the case of improper user names it will state "improper username or password"
        3. The four pages along with an initial page will display a visually appealing
        4. Any errors within the databse or system will be displayed base on the problem IE if the stock market is closed, it will show the pervious days price
        5. An error code for improper area code, symbol, and stock names will show with an error.
        6. Would like to be able to log in with Google
        7. Will save the data imputed from every users unique log in information.
        8. Will save up to 5 stock symbols for the user
        9. Each stock symbol will show current price
        10.Would like each symbol will have a "bought at" and "number of shares" input and will do the math to show you how much you have made
        11. Weather page will input users zipcode/city to show 5 day weather forcast
        12. Would like to use GPS within the computer to locate user
        13. Traffic will use same location as weather but show google traffic in their current area.
        14. Would like to have a direct link to google maps so that you can see their interactive maps
        15. Top 5 News Stories will import the top 5 news stories from a reputable website
        16. The news stories will have a headline, photo, and link to the information.

### Tracer Bullet
Demonstrate the use of all technologies you will use in your web application

1. At a minimum, demonstrate the ability to pull information from a *MySQL* or *mongoDB* database using *express.js* routes served using *node.js* to a client page enhanced with *Angular.js*
    a.
2. Each team member should provide a personal reflection that states at a minimum their contribution to this phase of the project, what they learned or found challenging, and what their concerns are for the next phase of development.
    a. Matt Walls imported data from a webpage to be used in the projects website. This was not as hard as originally intended. There were lots and lots of explanations online which only made it take a little while.
       He was able to import the 5 Top News Stories easily, but may have trouble in manipulating the data for the other webpages
    b. Alex Ververis created the ERD for the project to show the connection between everything. He did not find this too chanllenging but has to limit the amount of information actually stored in the databse verse
       what was imported from the website files. Moving on he will be creating a secure log in (hopefully through google) and will find issues in saving, securing, and recalling all of the users information.
    c. Terrance Oliveri-Williams has created some of the requirements for the project and worked on making the webpages have a user friendly system for saving and getting information. Next he will start working on putting the pieces together
       so that the information shows for their specific needs. For instance it will show that they have 5 stocks and how much they have made off of those stocks.

3. Submit this information in the appropriate parts of your README.md and in the appropriate directories (see [Submission Requirements](#markdown-header-submission-requirements)) ofyour final project git repository.
    1. Your *git* tag should be `tracer` and your tag message should be `submit tracer bullet`
    2. Your *git* commit message should be `submit tracer bullet`
    3. The following are sections of the README.md file that you should update (see [Attachment 1](#markdown-header-attachment-1)):
        + Tracer Bullet Documentation
        + Tracer Bullet Personal Reflections
        + Dependencies

### Team Member reflection
1. Matthew Walls. He created the entire front end of the project to make the website look pretty and have some functionality. Additionally
	he went to EI multiple times(4 total) in order to learn and understand how the backend of mongo databases works.He worked on linking the back end to the html 
	pages so that the user to could see and display the information they are seeking
2.ALex Ververis made huge contributions on the backend of the project. He created mulitple extra files, looked up tons of infomration, and wrote a good portion of the 
	backend for the mongo database so that the math and importation of information ran smoothly. 
3. Terrance Oliveri-Williamsdid a lot of the clean up for this project. Once things were somewhat working, he finished them off so that everything worked nicely 
	and ran smoothly. 

### Suggested/Wanted Grades
1. Matthew Walls - could care less. ANYTHING ABOVE a 75 would be all I want! 
2. Alex Ververis - Believes he deserves a 85 in the class for his hard work and high test scores
3. Terrance Oliveri-Williams- believes he deserves an 80 or above in the class. 